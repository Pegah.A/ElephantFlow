from __future__ import division
import numpy as np
import math

num_in_each_class = np.zeros(6)
selected_flows_path = "/u/pegahabed/elephantflow/files/selected_flows_detect.txt"
selected_flows_file = open(selected_flows_path, "w")

selected_flows_complete_info_path= "/u/pegahabed/elephantflow/files/selected_flows_complete_info_detect.txt"
selected_flows_complete_info_file = open(selected_flows_complete_info_path, "w")

selected_flows_output_predict_path = "/u/pegahabed/elephantflow/files/selected_flows_output_detect.txt"
selected_flows_output_predict_file = open(selected_flows_output_predict_path, "w")

def read_flows():

    data_path = "/u/pegahabed/elephantflow/files/inputdata.txt"
    data_file = open(data_path, "r")
    all_done = False

    first_line = data_file.readline()  # column titles
    for i in range(20000):  # we are discarding the first 20000 flows
        curr_flow = data_file.readline()
    #num_in_each_class = np.zeros(6)  # so far we have 0 from each class
    while all_done is not True:
        curr_flow = data_file.readline()
        curr_flow_array = curr_flow.split("\t")


        if (len(curr_flow_array) < 19):
            # print "I'll skip this"
            continue
        # All the possible information of the current flow
        pkt_count = curr_flow_array[12]

        if int(pkt_count) >= 5:
            print num_in_each_class

            all_done = slice_and_pick_flows_detect(curr_flow)
        else:
            # print ("i am ignoring small flows")  # We are ignoring all the mice flows with less than 5 packets
            continue



def slice_and_pick_flows_predict(curr_flow):

    curr_flow_array = curr_flow.split("\t")
    packets = curr_flow.split(";")
    rate_interval = 5  # looking at the packets in the next five seconds
    num_per_class = 100 #change to 1000
    num_packets_per_row = 5  # we have the info of 5 packets in each row of the training data

    flow_id = curr_flow_array[0]
    protocol = curr_flow_array[1]
    src_ip_1 = curr_flow_array[2]
    src_ip_2 = curr_flow_array[3]
    src_ip_3 = curr_flow_array[4]
    src_ip_4 = curr_flow_array[5]
    dst_ip_1 = curr_flow_array[6]
    dst_ip_2 = curr_flow_array[7]
    dst_ip_3 = curr_flow_array[8]
    dst_ip_4 = curr_flow_array[9]
    src_port = curr_flow_array[10]
    dst_port = curr_flow_array[11]
    pkt_count = curr_flow_array[12]
    flow_byte_sum = curr_flow_array[13]
    flow_l4_data_sum = curr_flow_array[14]
    flow_start_time = curr_flow_array[15]
    flow_end_time = curr_flow_array[16]
    flow_duration = curr_flow_array[17]
    flow_cumulative_tcp_flags = curr_flow_array[18]

    group_count = int(pkt_count) - num_packets_per_row + 1  # group_count shows how many rows would be build from this flow based on the number of packets it has
    start_index = 1
    end_index = num_packets_per_row

    for j in range(group_count):
        rate_counter = 0
        include_this = False
        curr_pkt_curr_time = packets[end_index].split("\t")[2]


        for m in range(end_index + 1, len(packets)):
            if float(packets[m].split("\t")[2]) - float(
                    curr_pkt_curr_time) < rate_interval:  # checking arrival time difference and determining rate
                rate_counter = rate_counter + 1

        #print "rate is: ", rate_counter
        if rate_counter >= 0 and rate_counter <= 1:  # class 1 of possible output: 0-1
            if num_in_each_class[0] < num_per_class:
                selected_flows_output_predict_file.write(str(1))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[0] = num_in_each_class[0] + 1
                include_this = True

        elif rate_counter > 1 and rate_counter <= 5:  # class 2 of possible output: 1-5
            if num_in_each_class[1] < num_per_class:
                selected_flows_output_predict_file.write(str(2))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[1]= num_in_each_class[1] + 1
                include_this = True


        elif rate_counter > 5 and rate_counter <= 20:  # class 3 of possible output: 5-20
            if num_in_each_class[2] < num_per_class:
                selected_flows_output_predict_file.write(str(3))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[2] = num_in_each_class[2] + 1
                include_this = True


        elif rate_counter > 20 and rate_counter <= 50:  # class 4 of possible output: 20-50
            if num_in_each_class[3] < num_per_class:
                selected_flows_output_predict_file.write(str(4))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[3] = num_in_each_class[3] + 1
                include_this = True


        elif rate_counter > 50 and rate_counter <= 100:  # class 5 of possible output: 50-100
            if num_in_each_class[4] < num_per_class:
                selected_flows_output_predict_file.write(str(5))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[4] = num_in_each_class[4] + 1
                include_this = True


        else:
            if num_in_each_class[5] < num_per_class:
                selected_flows_output_predict_file.write(str(6))  # class 6 of possible output: > 100
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[5] = num_in_each_class[5] + 1
                include_this = True

        all_done = all(i >=num_per_class for i in num_in_each_class)

        if include_this == True:

            #selected_flows_complete_info_file.write(curr_flow)
            #selected_flows_complete_info_file.write("\n")

            # Normalizing protocol: dividing it by 10
            selected_flows_file.write(str(float(protocol) / 10))  # flow protocol
            selected_flows_file.write("\t")

            # Normalizing source and destination port number
            # if <1024, map to 0 - 0.75
            # if >1024, map to 0.75 - 1
            if int(src_port) < 1024:
                normalized_src_port = int(src_port) / 10000
                normalized_src_port = normalized_src_port * 0.75
            elif int(src_port) >= 1024:
                normalized_src_port = int(src_port) / 10000
                normalized_src_port = (normalized_src_port * 0.25) + 0.75

            selected_flows_file.write(str(normalized_src_port))  # flow source port
            selected_flows_file.write("\t")


            if int(dst_port) < 1024:
                normalized_dst_port = int(dst_port) / 10000
                normalized_dst_port = normalized_dst_port * 0.75
            elif int(dst_port) >= 1024:
                normalized_dst_port = int(dst_port) / 10000
                normalized_dst_port = (normalized_dst_port * 0.25) + 0.75

            selected_flows_file.write(str(normalized_dst_port))  # flow destination port
            selected_flows_file.write("\t")

            # Normalizing source and destination ip address: dividing by 256
            selected_flows_file.write(str(float(src_ip_4) / 256))  # flow source ip address part 4
            selected_flows_file.write("\t")

            selected_flows_file.write(str(float(dst_ip_4) / 256))  # flow destination ip address part 4
            selected_flows_file.write("\t")


            for k in range(start_index, end_index + 1):

                # Normalizing packet arrival time: by using arctg / pi
                selected_flows_file.write(str(math.atan(float(packets[k].split("\t")[2])) / math.pi))  # packet arrival time
                selected_flows_file.write("\t")

                # Normalizing packet inter-arrival time: by using arctg / pi
                selected_flows_file.write(str(math.atan(float(packets[k].split("\t")[3])) / math.pi))  # packet inter-arrival time
                selected_flows_file.write("\t")

                # Normalizing packet size: by dividing it by 1500
                selected_flows_file.write(str(int(packets[k].split("\t")[4]) / 1500))  # packet size
                selected_flows_file.write("\t")

                # Normalizing packet layer 4 data size: by dividing it by 1500
                selected_flows_file.write(str(int(packets[k].split("\t")[5]) / 1500))  # packet l4 data size
                selected_flows_file.write("\t")

            selected_flows_file.write("\n")
        start_index += 1
        end_index += 1

    #selected_flows_file.close()
    return all_done



def slice_and_pick_flows_detect(curr_flow):

    curr_flow_array = curr_flow.split("\t")
    packets = curr_flow.split(";")
    rate_interval = 5  # looking at the packets in the last five seconds
    num_per_class = 1000 #change to 1000
    num_packets_per_row = 5  # we have the info of 5 packets in each row of the training data

    flow_id = curr_flow_array[0]
    protocol = curr_flow_array[1]
    src_ip_1 = curr_flow_array[2]
    src_ip_2 = curr_flow_array[3]
    src_ip_3 = curr_flow_array[4]
    src_ip_4 = curr_flow_array[5]
    dst_ip_1 = curr_flow_array[6]
    dst_ip_2 = curr_flow_array[7]
    dst_ip_3 = curr_flow_array[8]
    dst_ip_4 = curr_flow_array[9]
    src_port = curr_flow_array[10]
    dst_port = curr_flow_array[11]
    pkt_count = curr_flow_array[12]
    flow_byte_sum = curr_flow_array[13]
    flow_l4_data_sum = curr_flow_array[14]
    flow_start_time = curr_flow_array[15]
    flow_end_time = curr_flow_array[16]
    flow_duration = curr_flow_array[17]
    flow_cumulative_tcp_flags = curr_flow_array[18]

    group_count = int(pkt_count) - num_packets_per_row + 1  # group_count shows how many rows would be build from this flow based on the number of packets it has
    start_index = 1
    end_index = num_packets_per_row

    for j in range(group_count):
        rate_counter = 0
        include_this = False
        curr_pkt_curr_time = packets[end_index].split("\t")[2]


        for m in range(end_index):
            if float(float(curr_pkt_curr_time)- float(packets[m].split("\t")[2]))< rate_interval:  # checking arrival time difference and determining rate
                rate_counter = rate_counter + 1

        #print "rate is: ", rate_counter
        if rate_counter >= 0 and rate_counter <= 1:  # class 1 of possible output: 0-1
            if num_in_each_class[0] < num_per_class:
                selected_flows_output_predict_file.write(str(1))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[0] = num_in_each_class[0] + 1
                include_this = True

        elif rate_counter > 1 and rate_counter <= 5:  # class 2 of possible output: 1-5
            if num_in_each_class[1] < num_per_class:
                selected_flows_output_predict_file.write(str(2))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[1]= num_in_each_class[1] + 1
                include_this = True


        elif rate_counter > 5 and rate_counter <= 20:  # class 3 of possible output: 5-20
            if num_in_each_class[2] < num_per_class:
                selected_flows_output_predict_file.write(str(3))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[2] = num_in_each_class[2] + 1
                include_this = True


        elif rate_counter > 20 and rate_counter <= 50:  # class 4 of possible output: 20-50
            if num_in_each_class[3] < num_per_class:
                selected_flows_output_predict_file.write(str(4))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[3] = num_in_each_class[3] + 1
                include_this = True


        elif rate_counter > 50 and rate_counter <= 100:  # class 5 of possible output: 50-100
            if num_in_each_class[4] < num_per_class:
                selected_flows_output_predict_file.write(str(5))
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[4] = num_in_each_class[4] + 1
                include_this = True


        else:
            if num_in_each_class[5] < num_per_class:
                selected_flows_output_predict_file.write(str(6))  # class 6 of possible output: > 100
                selected_flows_output_predict_file.write("\n")
                num_in_each_class[5] = num_in_each_class[5] + 1
                include_this = True

        all_done = all(i >=num_per_class for i in num_in_each_class)

        if include_this == True:

            #selected_flows_complete_info_file.write(curr_flow)
            #selected_flows_complete_info_file.write("\n")

            # Normalizing protocol: dividing it by 10
            selected_flows_file.write(str(float(protocol) / 10))  # flow protocol
            selected_flows_file.write("\t")

            # Normalizing source and destination port number
            # if <1024, map to 0 - 0.75
            # if >1024, map to 0.75 - 1
            if int(src_port) < 1024:
                normalized_src_port = int(src_port) / 10000
                normalized_src_port = normalized_src_port * 0.75
            elif int(src_port) >= 1024:
                normalized_src_port = int(src_port) / 10000
                normalized_src_port = (normalized_src_port * 0.25) + 0.75

            selected_flows_file.write(str(normalized_src_port))  # flow source port
            selected_flows_file.write("\t")


            if int(dst_port) < 1024:
                normalized_dst_port = int(dst_port) / 10000
                normalized_dst_port = normalized_dst_port * 0.75
            elif int(dst_port) >= 1024:
                normalized_dst_port = int(dst_port) / 10000
                normalized_dst_port = (normalized_dst_port * 0.25) + 0.75

            selected_flows_file.write(str(normalized_dst_port))  # flow destination port
            selected_flows_file.write("\t")

            # Normalizing source and destination ip address: dividing by 256
            selected_flows_file.write(str(float(src_ip_4) / 256))  # flow source ip address part 4
            selected_flows_file.write("\t")

            selected_flows_file.write(str(float(dst_ip_4) / 256))  # flow destination ip address part 4
            selected_flows_file.write("\t")


            for k in range(start_index, end_index + 1):

                # Normalizing packet arrival time: by using arctg / pi
                selected_flows_file.write(str(math.atan(float(packets[k].split("\t")[2])) / math.pi))  # packet arrival time
                selected_flows_file.write("\t")

                # Normalizing packet inter-arrival time: by using arctg / pi
                selected_flows_file.write(str(math.atan(float(packets[k].split("\t")[3])) / math.pi))  # packet inter-arrival time
                selected_flows_file.write("\t")

                # Normalizing packet size: by dividing it by 1500
                selected_flows_file.write(str(int(packets[k].split("\t")[4]) / 1500))  # packet size
                selected_flows_file.write("\t")

                # Normalizing packet layer 4 data size: by dividing it by 1500
                selected_flows_file.write(str(int(packets[k].split("\t")[5]) / 1500))  # packet l4 data size
                selected_flows_file.write("\t")

            selected_flows_file.write("\n")
        start_index += 1
        end_index += 1

    #selected_flows_file.close()
    return all_done


read_flows()
