from __future__ import division
import numpy as np

def build_confusion_matrices(features_list):
    predictions_path = "/u/pegahabed/elephantflow/files/predictions.txt" # the predicted results of our model
    predictions_file = open(predictions_path, "r+")
    train_output_rate_path = "/u/pegahabed/elephantflow/files/train_output_predict.txt" #the actual true classes of the training data
    train_output_file = open(train_output_rate_path, "r")
    train_output_file_temp = open(train_output_rate_path, "r")

    confusion_matrices_path  = "/u/pegahabed/elephantflow/files/confusion_matrices.txt"
    confusion_matrices_file = open(confusion_matrices_path, "a")


    total_series_length = len(train_output_file_temp.readlines())
    total_series_length = (total_series_length // 100) * 100  # we are doing this so we would always be using a 10* number of rows => batch_size 10 is divisable
    num_classes = 6
    
    print "in the confusion matrix"
    #print " total_series_length  is:" , total_series_length
    confusion_matrix = np.zeros([num_classes, num_classes])
    for i in range(total_series_length):
        predicted_res = int(predictions_file.readline().split("\n")[0]) - 1  # minus 1 because they need to be array index starting from 0
        actual_res = int(train_output_file.readline().split("\n")[0]) - 1  # minus 1 because they need to be array index starting from 0

        confusion_matrix[predicted_res, actual_res] = confusion_matrix[predicted_res, actual_res] + 1

    percentage_confusion_matrix = confusion_matrix / sum(confusion_matrix)
    sum_diagonal_values = sum(percentage_confusion_matrix.diagonal())
    digonal_avg = sum_diagonal_values / 6
    avg_class_5_6_values =  (percentage_confusion_matrix[4,4] + percentage_confusion_matrix[5,5]) / 2


    #print "The Confusion Matrix is: \n " , confusion_matrix
    #print "\n\n"
    #print "The total number of flows for each class is: \n" , sum(confusion_matrix)
    #print "\n\n"
    #print "The relative Confusion Matrix is: \n",percentage_confusion_matrix

    confusion_matrices_file.write(str(features_list))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(confusion_matrix))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(sum(confusion_matrix)))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(percentage_confusion_matrix ))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(digonal_avg))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(avg_class_5_6_values))
    confusion_matrices_file.write("\n")

    print "writing the confusion matrices part done"
    #confusion_matrices_file.close()
