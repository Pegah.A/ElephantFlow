# Remembers the input data and echoes it after a few time-steps.
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

 
def EchoRNN2main(num_of_features):
    train_path = "/u/pegahabed/elephantflow/files/traindata.txt"
    train_output_rate_path = "/u/pegahabed/elephantflow/files/train_output_predict.txt"
    train_output_file= open(train_output_rate_path,"r")
    input_file= open(train_path, "r")

    predictions_path = "/u/pegahabed/elephantflow/files/predictions.txt"
    predictions_file = open(predictions_path, "w")

    num_epochs = 200
    total_series_length = len(input_file.readlines())
    #total_series_length = num_of_lines
    print "total is ", total_series_length
    total_series_length = (total_series_length // 10) * 10
    truncated_backprop_length = 10
    state_size = 4
    num_classes = 6
    echo_step = 3
    batch_size = 10
    num_batches = total_series_length//batch_size//truncated_backprop_length

    tf.reset_default_graph()
    #generating the training data  input and output   I have this data from the network
    # the input is random binary vector
    # the output will be the echo of the input, shifted echo_step steps to the right

    def generateData():

        train_path = "/u/pegahabed/elephantflow/files/traindata.txt"
        train_output_rate_path = "/u/pegahabed/elephantflow/files/train_output_predict.txt"

        x_file = open(train_path, "r")
        y_file = open(train_output_rate_path, "r")



        #total_series_length = 3883# len(x_file.readlines())


        #num_batches = total_series_length // batch_size // truncated_backprop_length


        x_main_array = np.empty([total_series_length, num_of_features])  #xxxxx
        #y_main_array = np.zeros([total_series_length, num_classes])

        for i in range (total_series_length): #len(temp_read)): # for each line of the file,
            x_line_flt_arr = []
            x_line_str_arr = x_file.readline().split("\t")  # read that line and split by tab  you will have a list of str
            for j in range (len(x_line_str_arr)-1): # for each element in this list which is str,
                x_line_flt_arr.append(float(x_line_str_arr[j])) # cast the element from str to float

            x_main_array[i] = x_line_flt_arr

            #label_value = int(y_file.readline().split("\n")[0])
            #y_main_array[i][label_value-1]= 1


        y_main_list=[]
        y_array = np.array(y_file.read().split("\n"))
        for i in range (total_series_length): #y_array.shape[0]-1):
            y_main_list.append(int(y_array[i])-1)

        y_main_array = np.array(y_main_list)


    # the reshapin takes the whole dataset and puts it into a matrix, that later will be sliced up into these mini-batches


    # put data into matrix of 5* -1  ( -1 -> 50000/5 = 10000)
        #x = x_main_array.reshape((batch_size, -1))
        #x= x_main_array.reshape(batch_size,total_series_length/batch_size,37)

        x= tf.reshape(x_main_array, [batch_size, -1, num_of_features])  # dividing the training data into mini batches  the return type will be a tensor xxxx
        x = x.eval()  # by using eval, we convert from tensor to numpy array

        y = y_main_array.reshape((batch_size, -1))
        return (x,y)


    # On each run the batch data is fed to the placeholders. Also, the RNN-state is supplied in a placeholder
    #which is saved from the output of the previous run.

    #placeholder: Inserts a placeholder for a tensor that will be always fed -  dtype, shape=None, name=None
    #dtype: the type of elements in the tensor to be fed

    batchX_placeholder = tf.placeholder(tf.float32, [batch_size, truncated_backprop_length, num_of_features ]) # xxxx
    batchY_placeholder = tf.placeholder(tf.int32, [batch_size, truncated_backprop_length])

    init_state = tf.placeholder(tf.float32, [batch_size, state_size])

    # when you train a model, you use variables to hold and update parameters. Variables are in-memory buffers containing tensors.
    # They must be explicitly initialized and can be saved to disk during and after training. You can later restore
    # saved values to exercise or analyze model.

    # TensorFlow provides a collection of ops that produce tensors often used for initialization from constants or random values.

    # You should specify the shape of the tensors. Variables generally have a fixed size.

    w = tf.Variable(np.random.rand(state_size + num_of_features, state_size), dtype = tf.float32) #XXXXX
    b = tf.Variable(np.zeros((1,state_size)), dtype = tf.float32)

    w2 = tf.Variable(np.random.rand(state_size, num_classes), dtype = tf.float32)
    b2 = tf.Variable(np.zeros((1, num_classes)), dtype = tf.float32)


    #unpack columns:
    #axis = 1 is for columns   axis = 0 is rows

    #unpacking the columns of the batch into a Python list.
    # a time series thing is like a vertical thing. The RNN will simultaneously be training on
    #different parts in the time-series.  The number of these different parts == batch_size
    # => we need #"batch_size" instances of states when propagating forward.
    # you can this in the fact that init_state has batch_size number of rows.

    # output of unpack : the list of Tensor objects
    # unpack has been renamed to unstack
    inputs_series = tf.unstack(batchX_placeholder, axis = 1)
    labels_series = tf.unstack(batchY_placeholder, axis = 1)
    # the actual RNN
    #Forward pass

    current_state = init_state
    states_series = []

    # input_series is a list of tensors
    for current_input in inputs_series:
        # the type of current input is a tensor
        #current_input = tf.reshape(current_input, [batch_size, 1, 37])

        input_and_state_concatenated = tf.concat(1,[current_input, current_state]) #increasing # of columns

    # we are using the same weight for both input and state(?)
    # if we want to use different weights, simply concat for the weights as well(?)

        # is next_state the output of this state as well?

        next_state = tf.tanh(tf.matmul(input_and_state_concatenated, w)+b)
        states_series.append(next_state)
        current_state = next_state

    #final part of the graph, a fully connected softmax layer from the state to the output that will
    #make the classes one-hot encoded and then calculating the loss of the batch.

    # logits_series is a list of tensors
    logits_series = [tf.matmul(state, w2)+b2 for state in states_series]

    #print("shape of logits_series is: ", len(logits_series))
    #for current_logit in logits_series:
        #print("shape of current logit is: ", current_logit.get_shape())

    # prediction_series is a list of tensors
    predictions_series = [tf.nn.softmax(logits) for logits in logits_series]
    #for current_predict in predictions_series:
        #print("type of current predict is: ",current_predict.get_shape())



    #calculates the softmax internally and then computes the cross entropy
    # sparse-softmax because output classes are mutually exclusive
    losses = [tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,labels=labels) for logits, labels in zip(logits_series, labels_series)]
    total_loss = tf.reduce_mean(losses)
    train_step = tf.train.AdagradOptimizer(0.5).minimize(total_loss)  # this number is the learning rate. Play with it.


    def plot(loss_list, predictions_series, batchX, batchY):
        plt.subplot(2,3,1)
        plt.cla()
        plt.plot(loss_list)

        for batch_series_idx in range(5):
            one_hot_output_series = np.array(predictions_series)[:,batch_series_idx,:]
            single_output_series = np.array([(1 if out[0]<0.5 else 0) for out in one_hot_output_series])

            plt.subplot(2,3,batch_series_idx+2)
            plt.cla()
            plt.axis([0,truncated_backprop_length, 0, 2])
            left_offset = range(truncated_backprop_length)
            #plt.bar(left_offset, batchX[batch_series_idx,:], width = 1, color = "blue")
            #plt.bar(left_offset, batchY[batch_series_idx,:]*0.5, width = 1, color = "red")
            plt.bar(left_offset, single_output_series * 0.3, width = 1, color = "green")

        plt.draw()
        plt.pause(0.0001)
 
    with tf.Session() as sess:
        
        #sess.run(tf.initialize_all_variables())
        print "calling the tf session from somewehere else"
        #predictions_file.truncate()
        sess.run(tf.global_variables_initializer())
        #plt.ion()
        #plt.figure()
        #plt.show()
        loss_list = []



        for epoch_idx in range(num_epochs):
            #print("epoch: ", epoch_idx)
            batch_loss_list = []
            predict_list = []

            x, y = generateData()
            _current_state = np.zeros((batch_size,state_size))


            for batch_idx in range(num_batches):

                #print("batch: ", batch_idx)
                start_idx = batch_idx * truncated_backprop_length
                end_idx = start_idx + truncated_backprop_length

                batchX = x[:,start_idx:end_idx,:]
                batchY = y[:,start_idx:end_idx]

                _total_loss,_train_step, _current_state, _predictions_series = sess.run([total_loss, train_step, current_state, predictions_series],
                                                                                        feed_dict= {
                                                                                            batchX_placeholder:batchX,
                                                                                            batchY_placeholder:batchY,
                                                                                            init_state:_current_state
                                                                                        })
                batch_loss_list.append(_total_loss)


                mean_batch_loss = sum(batch_loss_list)/len(batch_loss_list)
                counter = 0
                for i in range (len(_predictions_series)):
                    for j in range (len(_predictions_series[i])):
                        predict_list.append(_predictions_series[i][j])
                        counter +=1
       
        #sess.close()




                #if batch_idx%10 == 0:
            #print ("Loss", _total_loss)
                #plot(loss_list, _predictions_series, batchX, batchY)
            #print("length of prediction series is: ", len(predict_list))

            #print("The info about this epoch, the mean loss is: ", mean_batch_loss)
            #print("length of predict list is: ", len(predict_list))
        #for i in range(len(predict_list)):
            #predictions_file.write(str(predict_list[i]))
            #predictions_file.write("\n")


        for i in range(len(predict_list)):
            predictions_file.write(str(np.argmax(predict_list[i])+1))
            predictions_file.write("\n")




        #plt.ioff()
        #plt.show()

#EchoRNN2main(2)
