import itertools
from EchoRNN2 import EchoRNN2main
from confusion_matrix import build_confusion_matrices
import operator as op

features_path = "/u/pegahabed/elephantflow/files/features.txt"
features_file = open(features_path, "r")



selected_flows_path = "/u/pegahabed/elephantflow/files/selected_flows_detect.txt"
selected_flows_file = open(selected_flows_path, "r").readlines()

selected_flows_output_predict_path = "/u/pegahabed/elephantflow/files/selected_flows_output_detect.txt"
selected_flows_output_predict_file = open(selected_flows_output_predict_path, "r").readlines()

line_track_path = "/u/pegahabed/elephantflow/files/line_track.txt"
line_track_file = open(line_track_path, "w")

def make_training_input_output():
    num_packets_per_row = 5
    features_line_count = 1
    features = ["protocol", "src_port", "dst_port", "src_ip", "dst_ip", "pkt_arr_time", "pkt_inter_arr_time",
                "pkt_size", "pkt_l4_data_size"]
    #for i in range (500):
        #curr_features = features_file.readline().split("\n")[0].split("[")[1].split("]")[0].split(",")

    while features_line_count <512: #512:

        train_path = "/u/pegahabed/elephantflow/files/traindata.txt"
        train_file = open(train_path, "w")
        train_file.truncate()

        train_output_predict_path = "/u/pegahabed/elephantflow/files/train_output_predict.txt"
        train_output_predict_file = open(train_output_predict_path, "w")
        train_output_predict_file.truncate()


        curr_features = features_file.readline().split("\n")[0].split("[")[1].split("]")[0].split(",")
        print curr_features
        line_track_file.write(str(features_line_count))
        line_track_file.write("\n")
  	print "features line count is: ", features_line_count

        num_of_features = 0
        features_list = []
        for i in range(len(curr_features)):
            if int(curr_features[i]) == 1:
                features_list.append(features[i])
                if i<=4:
			num_of_features += 1
		elif i>4:
			num_of_features += num_packets_per_row


        for curr_flow in selected_flows_file:
            #print "stuck in loop?"
            #curr_flow = selected_flows_file.readline()
            #curr_flow_output= selected_flows_output_predict_file.readline().split("\n")[0]
            curr_flow_array = curr_flow.split("\t")

            for i in range(5): # 5 is the number of features related to the flow info
                if int(curr_features[i]) == 1:
                    train_file.write(curr_flow_array[i])
                    train_file.write("\t")


            flow_index = 5

            for i in range (num_packets_per_row):

                if int(curr_features[5]) == 1:
                    train_file.write(curr_flow_array[flow_index])
                    train_file.write("\t")

                if int(curr_features[6]) == 1:
                    train_file.write(curr_flow_array[flow_index+1])
                    train_file.write("\t")

                if int(curr_features[7]) == 1:
                    train_file.write(curr_flow_array[flow_index+2])
                    train_file.write("\t")

                if int(curr_features[8]) == 1:
                    train_file.write(curr_flow_array[flow_index+3])
                    train_file.write("\t")
                flow_index += 4  # 4 is the number of features related to packets info

            train_file.write("\n")

        for curr_flow_output in selected_flows_output_predict_file:
            train_output_predict_file.write(curr_flow_output)


        features_line_count +=1
        print "one round of features done"
        print features_list

        train_file.close()
        train_output_predict_file.close()
        EchoRNN2main(num_of_features)
        build_confusion_matrices(features_list)

        #raw_input("Press Enter to continue...")



make_training_input_output()
